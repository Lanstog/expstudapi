from data_process.dbConnection import DataBaseConnection
import rsa


class User:
    def __init__(self, user_data):
        user_ID = DataBaseConnection.get_session(user_data["ID"], True)
        user_data, role_data = DataBaseConnection.get_user_and_role(user_ID)
        self.edit_data = role_data[2]
        self.add_data = role_data[3]
        self.add_output_data = role_data[4]
        self.conduct_researches = role_data[6]
        self.ID = user_data[1]
        self.last_name = user_data[2]
        self.first_name = user_data[3]
        self.patronymic = user_data[4]


class Session:
    def __init__(self, ID, local_key, public_key, time):
        self.ID = ID
        self.local_key = local_key
        self.public_key = public_key
        self.time = time
