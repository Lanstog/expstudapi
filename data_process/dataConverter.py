import json
import csv

from classes.plan_entities.plan import Plan, PlanSecond
from data_process.dbConnection import DataBaseConnection
import pandas as pd

from validator import Validator


class DataConverter:
    @staticmethod
    async def save_data(request, user, project_file=False):
        json_struct = await DataConverter.json(request)
        if "data" not in json_struct:
            Validator.throw_exception(400, "No field: data")
        bytes_fromhex = bytes.fromhex(json_struct["data"])
        bytes_fromhex = bytes_fromhex.replace(b"\xda", b"\n")
        data = bytes_fromhex.decode("utf-8")
        ID = None
        if "ID" in json_struct:
            ID = json_struct["ID"]
        name = "No name"
        if "name" in json_struct:
            name = json_struct["name"]
        ID_response = DataBaseConnection.save_file(data, ID, user, project_file)
        DataBaseConnection.set_data_name(ID_response, name)
        return ID_response

    @staticmethod
    async def save_plan_data(request, user):
        json_struct = await DataConverter.json(request)
        ID = None
        if "ID" in json_struct["experiment"]:
            ID = json_struct["experiment"]["ID"]
        name = "No name"
        if "name" in json_struct:
            name = json_struct["name"]
        ID_response = DataBaseConnection.save_plan_data(json_struct, ID, user)
        DataBaseConnection.set_data_name(ID_response, name)
        return ID_response

    @staticmethod
    def add_data(data, fields, ID):
        count = DataConverter.get_add_size(fields['data'])
        new_rows = pd.DataFrame(columns=data.columns, index=range(0, count))
        for field in data.columns:
            if count is not None:
                for i in range(0, count):
                    obj = None
                    for j in range(0, len(fields['data'])):
                        if fields['data'][j]['name'] == field:
                            obj = fields['data'][j]['values']
                            break
                    new_rows[field][i] = obj[i]
            else:
                Validator.throw_exception(400, "Some data is missed", ID)
        data = data.append(new_rows)
        return data

    @staticmethod
    async def get_data(ID):
        data, csv = DataBaseConnection.get_data(ID)
        return data, csv

    @staticmethod
    def get_plan_data(ID):
        parameters, experiments, titles, a = DataBaseConnection.get_plan_data(ID)
        plan = Plan(parameters, experiments, titles, a)
        return plan

    @staticmethod
    def get_plan_data_second(ID):
        parameters, experiments, titles, a = DataBaseConnection.get_plan_data(ID)
        plan = PlanSecond(parameters, experiments, titles, a)
        return plan

    @staticmethod
    async def convert_data(json_struct):
        if "data_ID" not in json_struct:
            Validator.throw_exception(400, "No data with such ID")
        data, csv = await DataConverter.get_data(json_struct["data_ID"])
        data = data.split("\n")
        updated_data = []
        for line in data:
            line = line.split(",")
            updated_data.append(line)
        DataConverter.write_csv(updated_data, json_struct["data_ID"])
        csvfile = open("./csvFiles/file"+str(json_struct["data_ID"])+".csv", "r", newline="\n")
        reader = pd.read_csv("./csvFiles/file"+str(json_struct["data_ID"])+".csv")
        return reader, csvfile

    @staticmethod
    async def json(value, loads=json.loads):
        body = await value.text()
        return loads(body)

    @staticmethod
    def write_csv(data, ID):
        with open("./csvFiles/file"+str(ID)+".csv", "w+", newline='\n') as csv_file:
            writer = csv.writer(csv_file, delimiter=',')
            for line in data:
                writer.writerow(line)
            csv_file.close()
            return "./csvFiles/file.csv"

    @staticmethod
    def get_add_size(fields):
        length = len(fields[0]['values'])
        for i in range(1, len(fields)):
            if length != len(fields[i]['values']):
                return None
        return length
