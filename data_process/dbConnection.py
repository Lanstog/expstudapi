import psycopg2
from classes.models.model import Model
from server_config import ServerConfig
import datetime
import pytz
from validator import Validator


class DataBaseConnection:
    @staticmethod
    def create_db_connection():
        conn = psycopg2.connect(dbname=ServerConfig.db_name, user=ServerConfig.user,
                                password=ServerConfig.password, host=ServerConfig.host)
        cursor = conn.cursor()
        return conn, cursor

    @staticmethod
    def save_file(file, ID, user, project_file=False):
        conn, cursor = DataBaseConnection.create_db_connection()
        if ID == None:
            cursor.execute("BEGIN;")
            request = "INSERT INTO \"DataCSV\" (\"Data\") VALUES (\'" + file + "\')\n"
            cursor.execute(request)
            cursor.execute("SELECT \"ID\" FROM \"DataCSV\" ORDER BY \"ID\" DESC LIMIT 1")
            records = cursor.fetchall()
            ID = records[0][0]
            request = "INSERT INTO \"Data\" (\"DataCSVID\", \"User\", \"CreationDate\", \"LastFixDate\", \"ProjectFile\") VALUES (" + str(ID) + ", " + str(user.ID) + ", \'" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "\', \'" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "\', " + str(project_file) + ")\n"
            cursor.execute(request)
            cursor.execute("SELECT \"ID\" FROM \"Data\" ORDER BY \"ID\" DESC LIMIT 1")
            records = cursor.fetchall()
            ID = records[0][0]
            cursor.execute("COMMIT;")
        else:
            request = "UPDATE \"Data\" SET \"LastFixDate\" = \'" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "\' WHERE \"ID\" = " + str(ID)
            cursor.execute(request)
            conn.commit()
            request = "SELECT \"DataCSVID\" FROM \"Data\" WHERE \"ID\" = " + str(ID)
            cursor.execute(request)
            records = cursor.fetchall()
            ID = records[0][0]
            request = "UPDATE \"Data\" SET \"Data\" = \'" + file + "\', \"LastFixDate\" = \'" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "\' WHERE \"ID\" = " + str(ID)
            cursor.execute(request)
            conn.commit()
        cursor.close()
        conn.close()
        return ID

    @staticmethod
    def save_plan_data(data, ID, user):
        conn, cursor = DataBaseConnection.create_db_connection()
        cursor.execute("BEGIN;")
        request = "INSERT INTO \"ExperimentParameters\" (\"Name\", \"Lowlvl\", \"Highlvl\") VALUES "
        i = 0
        experiment_titles = []
        for parameter in data["experiment"]["parameters"]:
            if i is not 0:
                request += ","
            request += "(\'" + parameter["name"] + "\', " + str(parameter["lowlvl"]) + ", " + str(
                parameter["highlvl"]) + ")"
            i += 1
            experiment_titles.append(parameter["name"])
        cursor.execute(request)
        cursor.execute("SELECT \"ID\" FROM \"ExperimentParameters\" ORDER BY \"ID\" DESC LIMIT " + str(len(data["experiment"]["parameters"])))
        records = cursor.fetchall()
        IDParameters = records
        request = "INSERT INTO \"ExperimentsData\" (\"ExperimentOutputValues\", \"ExperimentParameters\") VALUES "
        i = 0
        for value in data["experiment"]["data"]:
            if i is not 0:
                request += ","
            request += "(\'{"
            j = 0
            for output in value["output"]:
                if j is not 0:
                    request += ","
                request += str(output)
                j += 1
            request += "}\', \'{"
            j = 0
            for output in value["parameters"]:
                if j is not 0:
                    request += ","
                request += str(output)
                j += 1
            request += "}\')"
            i += 1
        cursor.execute(request)
        cursor.execute("SELECT \"ID\" FROM \"ExperimentsData\" ORDER BY \"ID\" DESC LIMIT " + str(len(data["experiment"]["data"])))
        records = cursor.fetchall()
        IDData = records
        cursor.execute("COMMIT;")
        if ID == None:
            cursor.execute("BEGIN;")
            request = "INSERT INTO \"ExperimentPlanData\" (\"ParametersData\", \"ExperimentsData\", \"ExperimentTitles\", \"A\") VALUES (\'{"
            j = 0
            for output in reversed(IDParameters):
                if j is not 0:
                    request += ","
                request += str(output[0])
                j += 1
            request += "}\', \'{"
            j = 0
            for output in reversed(IDData):
                if j is not 0:
                    request += ","
                request += str(output[0])
                j += 1
            request += "}\', \'{"
            j = 0
            for output in data["experiment"]["titles"]:
                if j is not 0:
                    request += ","
                request += "\"" + str(output) + "\""
                j += 1
            request += "}\', " + str(data["a"]) + ")"
            cursor.execute(request)
            cursor.execute("SELECT \"ID\" FROM \"ExperimentPlanData\" ORDER BY \"ID\" DESC LIMIT 1")
            records = cursor.fetchall()
            ID = records[0][0]
            request = "INSERT INTO \"Data\" (\"ExperimentDataID\", \"User\", \"CreationDate\", \"LastFixDate\", \"ProjectFile\") VALUES (" + str(ID) + ", " + str(user.ID) + ", \'" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "\', \'" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "\', TRUE)\n"
            cursor.execute(request)
            cursor.execute("SELECT \"ID\" FROM \"Data\" ORDER BY \"ID\" DESC LIMIT 1")
            records = cursor.fetchall()
            ID = records[0][0]
            cursor.execute("COMMIT;")
        else:
            request = "UPDATE \"Data\" SET \"LastFixDate\" = \'" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "\' WHERE \"ID\" = " + str(ID)
            cursor.execute(request)
            conn.commit()
            request = "SELECT \"DataCSVID\" FROM \"Data\" WHERE \"ID\" = " + str(ID)
            cursor.execute(request)
            records = cursor.fetchall()
            ID = records[0][0]
            request = "UPDATE \"ExperimentPlanData\" SET \"ExperimentsData\" = \' {"
            j = 0
            for output in reversed(IDData):
                if j is not 0:
                    request += ","
                request += str(output[0])
                j += 1
            request += "}\', \"ParametersData\" = \'{"
            j = 0
            for output in reversed(IDParameters):
                if j is not 0:
                    request += ","
                request += str(output[0])
                j += 1
            request += "}\', \"ExperimentTitles\" = \'{"
            j = 0
            for output in data["experiment"]["titles"]:
                if j is not 0:
                    request += ","
                request += "\"" + str(output) + "\""
                j += 1
            request += "}\' WHERE \"ID\" = " + str(ID)
            cursor.execute(request)
            conn.commit()
        cursor.close()
        conn.close()
        return ID

    @staticmethod
    def get_plan_data(ID):
        conn, cursor = DataBaseConnection.create_db_connection()
        request = "SELECT \"ExperimentDataID\" FROM \"Data\" WHERE \"ID\" = " + str(ID)
        cursor.execute(request)
        records = cursor.fetchall()
        ID = records[0][0]
        request = "SELECT \"ParametersData\", \"ExperimentsData\", \"ExperimentTitles\", \"A\" FROM \"ExperimentPlanData\" WHERE \"ID\" = " + str(ID)
        cursor.execute(request)
        records = cursor.fetchall()
        titles = records[0][2]
        a = records[0][3]
        experiments = []
        parameters = []
        for table_id in records[0][0]:
            request = "SELECT \"Name\", \"Lowlvl\", \"Highlvl\" FROM \"ExperimentParameters\" WHERE \"ID\" = " + str(table_id)
            cursor.execute(request)
            par_records = cursor.fetchall()
            for record in par_records:
                parameters.append(record)
        for table_id in records[0][1]:
            request = "SELECT \"ExperimentOutputValues\", \"ExperimentParameters\" FROM \"ExperimentsData\" WHERE \"ID\" = " + str(table_id)
            cursor.execute(request)
            data_records = cursor.fetchall()
            for record in data_records:
                experiments.append(record)
        return parameters, experiments, titles, a

    @staticmethod
    def get_user_and_role(user_ID):
        conn, cursor = DataBaseConnection.create_db_connection()
        request = "SELECT \"UserRole\", \"ID\", \"LastName\", \"FirstName\", \"Patronymic\" FROM \"User\" WHERE \"ID\"=" + str(user_ID)
        cursor.execute(request)
        records = cursor.fetchall()
        if not len(records) == 1:
            Validator.throw_exception(401, "No such user")
        data = records[0][0]
        user_data = records[0]
        request = "SELECT * FROM \"UserRole\" WHERE \"ID\"=" + str(data)
        cursor.execute(request)
        records = cursor.fetchall()
        data = records[0]
        cursor.close()
        conn.close()
        return user_data, data

    @staticmethod
    def get_user_and_role_by_login(login, password):
        conn, cursor = DataBaseConnection.create_db_connection()
        request = "SELECT \"UserRole\", \"ID\", \"LastName\", \"FirstName\", \"Patronymic\" FROM \"User\" WHERE \"Login\"='" + str(login) + "' and \"Password\"='" + str(password) + "'"
        cursor.execute(request)
        records = cursor.fetchall()
        if not len(records) == 1:
            Validator.throw_exception(401, "No such user")
        data = records[0][0]
        user_data = records[0]
        request = "SELECT \"Description\", \"EditData\", \"AddData\", \"AddOutputData\", \"ConductResearches\", \"RoleName\" FROM \"UserRole\" WHERE \"ID\"=" + str(data)
        cursor.execute(request)
        records = cursor.fetchall()
        data = records[0]
        cursor.close()
        conn.close()
        return user_data, data

    @staticmethod
    def get_data(ID):
        conn, cursor = DataBaseConnection.create_db_connection()
        request = "SELECT \"DataCSVID\", \"ExperimentDataID\" FROM \"Data\" WHERE \"ID\" = " + str(ID)
        cursor.execute(request)
        records = cursor.fetchall()
        if records[0][0]:
            ID = records[0][0]
            request = "SELECT \"Data\" FROM \"DataCSV\" WHERE \"ID\"=" + str(ID)
            cursor.execute(request)
            records = cursor.fetchall()
            data = records[0][0]
            cursor.close()
            conn.close()
            return data, True
        else:
            ID = records[0][1]
            data = {}
            request = "SELECT \"ParametersData\", \"ExperimentsData\", \"ExperimentTitles\", \"A\" FROM \"ExperimentPlanData\" WHERE \"ID\"=" + str(ID)
            cursor.execute(request)
            records = cursor.fetchall()
            data.update({"alpha":  float(records[0][3]), "titles":  records[0][2]})
            parameters = []
            experiments = []
            parameters_ids = records[0][0]
            experiments_ids = records[0][1]
            for table_id in parameters_ids:
                request = "SELECT \"Name\", \"Lowlvl\", \"Highlvl\" FROM \"ExperimentParameters\" WHERE \"ID\" = " + str(
                    table_id)
                cursor.execute(request)
                par_records = cursor.fetchall()
                for record in par_records:
                    parameters.append({"name": record[0], "lowlvl": float(record[1]), "highlvl": float(record[2])})
            for table_id in experiments_ids:
                request = "SELECT \"ExperimentOutputValues\", \"ExperimentParameters\" FROM \"ExperimentsData\" WHERE \"ID\" = " + str(
                    table_id)
                cursor.execute(request)
                data_records = cursor.fetchall()
                for record in data_records:
                    output = []
                    for out in record[0]:
                        output.append(float(out))
                    param = []
                    for par in record[1]:
                        param.append(float(par))
                    experiments.append({"parameters": param, "output": output})
            data.update({'experiment': {"data": experiments, "parameters": parameters}})
            cursor.close()
            conn.close()
            return data, False

    @staticmethod
    def get_all_data(ID):
        conn, cursor = DataBaseConnection.create_db_connection()
        request = "SELECT \"ID\", \"LastFixDate\", \"CreationDate\" FROM \"Data\" WHERE \"User\"=" + str(ID) + " AND \"ProjectFile\"=TRUE ORDER BY \"LastFixDate\" DESC LIMIT 10"
        cursor.execute(request)
        records = cursor.fetchall()
        data = records
        cursor.close()
        conn.close()
        return data

    @staticmethod
    def create_empty_result(fields, method):
        conn, cursor = DataBaseConnection.create_db_connection()
        cursor.execute("BEGIN;")
        request = "INSERT INTO \"AnalyseResult\" (\"DataID\", \"AnalyseType\", \"Status\") VALUES (" + str(fields["data_ID"]) + ", \'" + method + "\', -1)"
        cursor.execute(request)
        cursor.execute("SELECT \"ID\" FROM \"AnalyseResult\" ORDER BY \"ID\" DESC LIMIT 1")
        records = cursor.fetchall()
        ID = records[0][0]
        cursor.execute("COMMIT;")
        cursor.close()
        conn.close()
        return ID

    @staticmethod
    def update_result(result, ID):
        conn, cursor = DataBaseConnection.create_db_connection()
        request = "UPDATE \"AnalyseResult\" SET \"Result\" = \'" + result + "\', \"Status\" = 0 WHERE \"ID\" = " + str(ID)
        cursor.execute(request)
        conn.commit()
        cursor.close()
        conn.close()

    @staticmethod
    def create_new_session(private_key, public_key):
        conn, cursor = DataBaseConnection.create_db_connection()
        cursor.execute("BEGIN;")
        request = "INSERT INTO \"Session\" (\"PublicKey\", \"PrivateKey\", \"TerminationTime\") VALUES (\'" + public_key + "\', \'" + private_key + "\', \'" + (datetime.datetime.now() + datetime.timedelta(minutes=ServerConfig.timeout)).strftime("%Y-%m-%d %H:%M:%S") + "\')"
        cursor.execute(request)
        cursor.execute("SELECT \"ID\" FROM \"Session\" ORDER BY \"ID\" DESC LIMIT 1")
        records = cursor.fetchall()
        ID = records[0][0]
        cursor.execute("COMMIT;")
        cursor.close()
        conn.close()
        return ID

    @staticmethod
    def get_session(ID, authorized = False):
        if not authorized:
            conn, cursor = DataBaseConnection.create_db_connection()
            cursor.execute("SELECT \"PublicKey\", \"PrivateKey\", \"TerminationTime\" FROM \"Session\" WHERE \"ID\" = \'" + str(ID) + "\'")
            records = cursor.fetchall()
            public_key = records[0][0]
            private_key = records[0][1]
            time = records[0][2]
            if time < pytz.utc.localize(datetime.datetime.utcnow()):
                Validator.throw_exception(401, "Session Terminated")
            request = "UPDATE \"Session\" SET \"TerminationTime\" = \'" + (datetime.datetime.now() + datetime.timedelta(minutes=ServerConfig.timeout)).strftime("%Y-%m-%d %H:%M:%S") + "\' WHERE \"ID\" = " + str(ID)
            cursor.execute(request)
            conn.commit()
            cursor.close()
            conn.close()
            for model in Model.models_array:
                if model.session_id == ID:
                    model.timeout = 3000
            return public_key, private_key
        else:
            conn, cursor = DataBaseConnection.create_db_connection()
            cursor.execute("SELECT \"UserID\", \"TerminationTime\" FROM \"Session\" WHERE \"ID\" = \'" + str(ID) + "\'")
            records = cursor.fetchall()
            user_ID = records[0][0]
            time = records[0][1]
            if time < pytz.utc.localize(datetime.datetime.utcnow()):
                Validator.throw_exception(401, "Session Terminated")
            request = "UPDATE \"Session\" SET \"TerminationTime\" = \'" + (datetime.datetime.now() + datetime.timedelta(minutes=ServerConfig.timeout)).strftime("%Y-%m-%d %H:%M:%S") + "\' WHERE \"ID\" = " + str(ID)
            cursor.execute(request)
            conn.commit()
            cursor.close()
            conn.close()
            return user_ID


    @staticmethod
    def activate_token(token_ID, user_ID):
        conn, cursor = DataBaseConnection.create_db_connection()
        request = "UPDATE \"Session\" SET \"TerminationTime\" = \'" + (datetime.datetime.now() + datetime.timedelta(minutes=ServerConfig.timeout)).strftime("%Y-%m-%d %H:%M:%S") + "\', \"Activated\" = TRUE, \"UserID\" = " + str(user_ID) + " WHERE \"ID\" = " + str(token_ID)
        cursor.execute(request)
        conn.commit()
        cursor.close()
        conn.close()

    @staticmethod
    def get_result(ID, user):
        conn, cursor = DataBaseConnection.create_db_connection()
        cursor.execute("SELECT \"Result\", \"Status\", \"AnalyseType\", \"DataID\" from \"AnalyseResult\" WHERE \"ID\" = \'" + str(ID) + "\'")
        records = cursor.fetchall()
        result = records[0][0]
        return result

    @staticmethod
    def save_model(coefs, data_ID):
        conn, cursor = DataBaseConnection.create_db_connection()
        cursor.execute("BEGIN;")
        request = "INSERT INTO \"Model\" (\"DataID\", \"ModelCoefs\") VALUES (\'" + str(data_ID) + "\', \'" + coefs + "\')"
        cursor.execute(request)
        cursor.execute("SELECT \"ID\" FROM \"Model\" ORDER BY \"ID\" DESC LIMIT 1")
        records = cursor.fetchall()
        ID = records[0][0]
        cursor.execute("COMMIT;")
        cursor.close()
        conn.close()
        return ID

    @staticmethod
    def get_model(ID):
        conn, cursor = DataBaseConnection.create_db_connection()
        cursor.execute("SELECT \"ModelCoefs\" from \"Model\" WHERE \"ID\" = \'" + str(ID) + "\'")
        records = cursor.fetchall()
        result = records[0][0]
        return result

    @staticmethod
    def get_analyze_results(ID):
        conn, cursor = DataBaseConnection.create_db_connection()
        cursor.execute("SELECT \"Result\", \"Status\", \"AnalyseType\", \"ID\" from \"AnalyseResult\" WHERE \"DataID\" = \'" + str(ID) + "\'")
        records = cursor.fetchall()
        analyse_results_types = []
        for val in records:
            exist = False
            for type in analyse_results_types:
                if type == val[2]:
                    exist = True
            if not exist:
                analyse_results_types.append(val[2])
        results = []
        for type in analyse_results_types:
            result = None
            cur_ID = 0
            for record in records:
                if type == record[2] and cur_ID < int(record[3]):
                    if (type == "remove_coll_cols" or type == "remove_unimportant_cols" or type == "replace_nulls" or type == "add_data" or type == "remove_outliers") and record[1] != -1:
                        cur_ID = int(record[3])
                        result = record[0]
                    elif not (type == "remove_coll_cols" or type == "remove_unimportant_cols" or type == "replace_nulls" or type == "add_data"):
                        cur_ID = int(record[3])
                        result = record[0]
            if result != None and cur_ID != 0:
                results.append({"ID": cur_ID, "name": type, "result": result})
        for result in results:
            if (result['name'] == "remove_coll_cols" or result['name'] == "remove_unimportant_cols" or result['name'] == "replace_nulls" or result['name'] == "add_data" or result['name'] == "remove_outliers") and result["result"] != None:
                response, csv = DataBaseConnection.get_data(result["result"]["ID"])
                analyze_results = DataBaseConnection.get_analyze_results(result["result"]["ID"])
                result["result"] = {"ID": result["result"]["ID"], "result": response.encode("utf-8").hex(), 'analyze_results': analyze_results}
        return results

    @staticmethod
    def get_data_csv_id(ID):
        conn, cursor = DataBaseConnection.create_db_connection()
        cursor.execute("SELECT \"DataCSVID\" from \"Data\" WHERE \"ID\" = \'" + str(ID) + "\'")
        records = cursor.fetchall()
        result = records[0][0]
        return result

    @staticmethod
    def get_data_name(ID):
        conn, cursor = DataBaseConnection.create_db_connection()
        cursor.execute("SELECT \"Name\" from \"Data\" WHERE \"ID\" = \'" + str(ID) + "\'")
        records = cursor.fetchall()
        result = records[0][0]
        return result

    @staticmethod
    def set_data_name(ID, name):
        conn, cursor = DataBaseConnection.create_db_connection()
        request = "UPDATE \"Data\" SET \"Name\" = \'" + name + "\' WHERE \"ID\" = " + str(ID)
        cursor.execute(request)
        conn.commit()
        cursor.close()
        conn.close()