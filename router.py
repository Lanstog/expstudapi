from aiohttp import web
from acceptor import Acceptor


class Router:
    @staticmethod
    def add_routes(app):
        app.add_routes([
            web.post('/find_min', Acceptor.get_min),
            web.post('/find_max', Acceptor.get_max),
            web.post('/find_mean', Acceptor.get_mean),
            web.post('/find_median', Acceptor.get_median),
            web.post('/find_dispersion', Acceptor.get_dispersion),
            web.post('/find_std', Acceptor.get_std),
            web.post('/find_corr', Acceptor.get_corr),
            web.post('/remove_coll_cols', Acceptor.remove_coll_cols),
            web.post('/remove_unimp_cols', Acceptor.remove_unimportant_cols),
            web.post('/save_data', Acceptor.save_file),
            web.post('/get_files_list', Acceptor.get_all_data),
            web.post('/get_data', Acceptor.get_data),
            web.post('/count_func', Acceptor.count_func),
            web.post('/build_reg', Acceptor.build_reg),
            web.post('/get_result', Acceptor.get_result),
            web.post('/fix_data', Acceptor.fix_data),
            web.get('/get_token', Acceptor.get_token),
            web.post('/save_plan_data', Acceptor.save_plan_data),
            web.post('/add_data', Acceptor.add_data),
            web.post('/activate_token', Acceptor.activate_token),
            web.post('/remove_outliers', Acceptor.remove_outliers)
        ])
        return app
