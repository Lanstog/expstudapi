import json

import psycopg2
from aiohttp import web
from server_config import ServerConfig


class Validator:
    @staticmethod
    def create_db_connection():
        conn = psycopg2.connect(dbname=ServerConfig.db_name, user=ServerConfig.user,
                                password=ServerConfig.password, host=ServerConfig.host)
        cursor = conn.cursor()
        return conn, cursor

    @staticmethod
    def throw_exception(code, text, ID=None):
        if ID is None:
            if code == 400:
                raise web.HTTPBadRequest(text=json.dumps({"code": code, "reason": text}))
            elif code == 401:
                raise web.HTTPUnauthorized(text=json.dumps({"code": code, "reason": text}))
            elif code == 403:
                raise web.HTTPForbidden(text=json.dumps({"code": code, "reason": text}))
            elif code == 500 and text != "":
                raise web.HTTPInternalServerError(text=json.dumps({"code": code, "reason": text}))
            else:
                raise web.HTTPInternalServerError(text="Unexpected error")
        else:
            result = json.dumps({"code": code, "reason": text})
            conn, cursor = Validator.create_db_connection()
            result = result.replace("'", "")
            request = "UPDATE \"AnalyseResult\" SET \"Result\" = \'" + result + "\', \"Status\" = " + str(code) + " WHERE \"ID\" = " + str(ID)
            cursor.execute(request)
            conn.commit()
            cursor.close()
            conn.close()
            raise Exception("Exception raised, ID=" + str(ID))
