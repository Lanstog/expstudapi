import copy


class Experiment:
    def __init__(self, output_values, parameters):
        self.output_values = output_values
        self.parameters = copy.deepcopy(parameters)
        start_size = len(self.parameters) - 1
        for i in range (2, len(parameters) + 1):
            array_indexes = []
            for j in range (0, i):
                array_indexes.append(j)
            current_index_switch = len(array_indexes) - 1
            while not current_index_switch == -1:
                add_val = 1
                for value in array_indexes:
                    add_val *= self.parameters[value]
                self.parameters.append(add_val)
                if i == start_size + 1:
                    break
                elif array_indexes[len(array_indexes)-1] == start_size:
                    if array_indexes[current_index_switch] == start_size - (len(array_indexes) - 1 - current_index_switch):
                        current_index_switch -= 1
                    array_indexes[current_index_switch] = array_indexes[current_index_switch] + 1
                    for j in range (current_index_switch + 1, len(array_indexes)):
                        array_indexes[j] = array_indexes[j - 1] + 1
                else:
                    array_indexes[len(array_indexes)-1] += 1


    def count_mean(self):
        result = 0
        for value in self.output_values:
            result += value
        result = result/len(self.output_values)
        return result

class ExperimentSecond:
    def __init__(self, output_values, parameters):
        self.output_values = output_values
        self.parameters = copy.deepcopy(parameters)
        self.quads = []
        start_size = len(self.parameters) - 1
        for i in range (2, len(parameters) + 1):
            array_indexes = []
            for j in range (0, i):
                array_indexes.append(j)
            current_index_switch = len(array_indexes) - 1
            while not current_index_switch == -1:
                add_val = 1
                for value in array_indexes:
                    add_val *= self.parameters[value]
                self.parameters.append(add_val)
                if i == start_size + 1:
                    break
                elif array_indexes[len(array_indexes)-1] == start_size:
                    if array_indexes[current_index_switch] == start_size - (len(array_indexes) - 1 - current_index_switch):
                        current_index_switch -= 1
                    array_indexes[current_index_switch] = array_indexes[current_index_switch] + 1
                    for j in range (current_index_switch + 1, len(array_indexes)):
                        array_indexes[j] = array_indexes[j - 1] + 1
                else:
                    array_indexes[len(array_indexes)-1] += 1
        for i in range(0, len(parameters)):
            self.quads.append(parameters[i]**2)


    def count_mean(self):
        result = 0
        for value in self.output_values:
            result += value
        result = result/len(self.output_values)
        return result