class Parameter:
    def __init__(self, name, lowlvl, highlvl):
        self.name = name
        self.lowlvl = lowlvl
        self.highlvl = highlvl

    def mean(self):
        return (self.highlvl + self.lowlvl)/2

    def interval(self):
        return (self.highlvl - self.lowlvl)/2
