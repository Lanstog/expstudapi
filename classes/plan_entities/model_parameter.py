class ModelParameter:
    def __init__(self, titles, variables, value):
        self.titles = titles
        self.variables = variables
        self.value = value

    def get_coef(self, titles):
        if self.titles[0] == "1" and titles[0] == "1":
            return self.value
        else:
            if self.get_count_of_titles(titles[0], titles) == 2 and self.get_count_of_titles(titles[0], self.titles) != 2:
                return None
            elif self.get_count_of_titles(titles[0], titles) == 2 and self.get_count_of_titles(titles[0], self.titles) == 2:
                var = self.get_var(titles[0], self.titles)
                result = self.value
                result *= 1/var.interval()**2
                return result
            for title in titles:
                exist = False
                for exist_title in self.titles:
                    if exist_title == title or title == "1":
                        exist = True
                        break
                if not exist:
                    return None
            result = self.value
            exist_array = []
            for exist_title in self.titles:
                stop = False
                for exist_val in exist_array:
                    if exist_val == exist_title:
                        stop = True
                if stop:
                    continue
                exist = 1
                for title in titles:
                    if exist_title == title:
                        exist -= self.get_count_of_titles(title, self.titles)
                        break
                var = self.get_var(exist_title, self.titles)
                if exist == 0:
                    result *= 1/var.interval()
                elif exist == 1:
                    result *= -1 * var.mean()/var.interval()
                elif exist == -1:
                    result *= -2 * var.mean()/(var.interval()**2)
                    exist_array.append(exist_title)
            return result

    def get_count_of_titles(self, title, titles):
        count = 0
        for exist_title in titles:
            if exist_title == title:
                count += 1
        return count

    def get_var(self, title, titles):
        for i in range(0, len(titles)):
            if title == titles[i]:
                return self.variables[i]
        return None
