from classes.plan_entities.experiment import Experiment, ExperimentSecond
from classes.plan_entities.parameter import Parameter
import numpy as np


class Plan:
    def __init__(self, parameters, experiments, titles, a):
        self.titles = titles
        self.parameters = []
        self.experiments = []
        self.a = float(a)
        for parameter in parameters:
            self.parameters.append(Parameter(parameter[0], parameter[1], parameter[2]))
        for experiment in experiments:
            self.experiments.append(Experiment(experiment[0], experiment[1]))

    def count_parameters(self):
        parameters = []
        value = 0
        for experiment in self.experiments:
            value += experiment.count_mean()
        parameters.append(value/len(self.experiments))
        for j in range(0, len(self.experiments[0].parameters)):
            value = 0
            for i in range(0, len(self.experiments)):
                value += self.experiments[i].count_mean() * self.experiments[i].parameters[j]
            parameters.append(value/len(self.experiments))
        return parameters


class PlanSecond:
    def __init__(self, parameters, experiments, titles, a):
        self.titles = titles
        self.parameters = []
        self.experiments = []
        self.a = float(a)
        for parameter in parameters:
            self.parameters.append(Parameter(parameter[0], parameter[1], parameter[2]))
        for experiment in experiments:
            self.experiments.append(ExperimentSecond(experiment[0], experiment[1]))
        count = len(self.experiments[0].quads)
        for i in range(0, count):
            array = []
            for experiment in self.experiments:
                array.append(experiment.quads[i])
            mean_val = np.mean(array)
            for experiment in self.experiments:
                experiment.parameters.append(experiment.quads[i] - mean_val)

    def count_parameters(self):
        parameters = []
        value = 0
        for experiment in self.experiments:
            value += experiment.count_mean()
        parameters.append(value/len(self.experiments))
        for j in range(0, len(self.experiments[0].parameters)):
            value = 0
            value_delim = 0
            for i in range(0, len(self.experiments)):
                value += self.experiments[i].count_mean() * self.experiments[i].parameters[j]
                value_delim += self.experiments[i].parameters[j] * self.experiments[i].parameters[j]
            parameters.append(value/value_delim)
        value = 0
        for i in range(0, len(self.experiments[0].quads)):
            array = []
            for experiment in self.experiments:
                array.append(experiment.quads[i])
            mean_val = np.mean(array)
            value -= mean_val * parameters[len(parameters) - (len(self.experiments[0].quads) - i)]
        parameters[0] += value
        return parameters