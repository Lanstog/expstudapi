class Model:
    models_array = []

    def __init__(self, model, id, session_id):
        self.id = id
        self.session_id = session_id
        self.model = model
        self.timeout = 3000
        self.models_array.append(self)

    def count_func(self, data):
        return self.model.predict(data).tolist()
