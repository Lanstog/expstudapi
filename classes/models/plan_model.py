import random

from classes.models.model import Model


class PlanModel(Model):

    def __init__(self, model, id, session_id):
        self.id = id
        self.session_id = session_id
        self.model = model
        self.timeout = 3000
        Model.models_array.append(self)

    def count_func(self, data):
        result = []
        for k in range(0, len(data.index)):
            for i in range(0, len(data.columns)):
                if not (isinstance(data.iloc[k][i], float) or isinstance(data.iloc[k][i], int)):
                    raise Exception("error")
            string_model = ""
            for i in range(0, len(self.model)):
                if self.model[i].titles[0] == "1":
                    string_model = string_model + " + " + str(self.model[i].value)
                else:
                    string_titles = ""
                    for j in range(0, len(self.model[i].titles)):
                        string_titles = string_titles + "data.iloc[" + str(k) + "][\"" + self.model[i].titles[j] + "\"]"
                        if not j == len(self.model[i].titles) - 1:
                            string_titles = string_titles + " * "
                    string_model = string_model + " + " + str(self.model[i].value) + " * " + str(string_titles)
            result.append(eval(string_model) + random.normalvariate(0, 1))
        return result