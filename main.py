import time
from threading import Thread

from aiohttp import web
from router import Router
from classes.models.model import Model
from server_config import ServerConfig


def check_models():
    while True:
        for model in Model.models_array:
            model.timeout = model.timeout - 1
            if model.timeout < 0:
                Model.models_array.remove(model)
        time.sleep(1)

if __name__ == '__main__':
    app = web.Application()
    app = Router.add_routes(app)
    print("Server is started")
    web.run_app(app, host=ServerConfig.server_host)
    thread = Thread(target=check_models())
    thread.start()
