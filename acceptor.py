import hashlib

from Crypto import Random
from aiohttp import web
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP

from classes.models.plan_model import PlanModel
from data_process.authorization import User
from classes.models.model import Model
from data_process.dataConverter import DataConverter
from functions.dataAnalyzer import DataAnalyzer
from functions.dataFixer import DataFixer
from functions.statisticFunctions import StatisticFunctions
from data_process.dbConnection import DataBaseConnection
from threading import Thread
import json
import os
import numpy as np
import rsa
import pandas as pd

from validator import Validator


class Acceptor:
    @staticmethod
    async def get_min(request):
        fields = await DataConverter.json(request)
        data, file = await DataConverter.convert_data(fields)
        user = User(fields["user"])
        if user.conduct_researches:
            ID = DataBaseConnection.create_empty_result(fields, "min")
            thread = Thread(target=Acceptor.get_min_con, args=(data, fields, ID, file))
            thread.start()
            response = {"ID": ID}
            return web.json_response(response)
        else:
            Validator.throw_exception(403, "No permission for conducting researches")

    @staticmethod
    def get_min_con(data, fields, ID, file):
        response = []
        for val in data.columns:
            response.append({"name": val, "value": StatisticFunctions.find_min(val, data)})
        Acceptor.remove_file(fields['data_ID'], file)
        response = {'result': response}
        DataBaseConnection.update_result(json.dumps(response), ID)

    @staticmethod
    async def get_max(request):
        fields = await DataConverter.json(request)
        data, file = await DataConverter.convert_data(fields)
        user = User(fields["user"])
        if user.conduct_researches:
            ID = DataBaseConnection.create_empty_result(fields, "max")
            thread = Thread(target=Acceptor.get_max_con, args=(data, fields, ID, file))
            thread.start()
            response = {"ID": ID}
            return web.json_response(response)
        else:
            Validator.throw_exception(403, "No permission for conducting researches")

    @staticmethod
    def get_max_con(data, fields, ID, file):
        response = []
        for val in data.columns:
            response.append({"name": val, "value": StatisticFunctions.find_max(val, data)})
        Acceptor.remove_file(fields['data_ID'], file)
        response = {'result': response}
        DataBaseConnection.update_result(json.dumps(response), ID)

    @staticmethod
    async def get_mean(request):
        fields = await DataConverter.json(request)
        data, file = await DataConverter.convert_data(fields)
        user = User(fields["user"])
        if user.conduct_researches:
            ID = DataBaseConnection.create_empty_result(fields, "mean")
            thread = Thread(target=Acceptor.get_mean_con, args=(data, fields, ID, file))
            thread.start()
            response = {"ID": ID}
            return web.json_response(response)
        else:
            Validator.throw_exception(403, "No permission for conducting researches")

    @staticmethod
    def get_mean_con(data, fields, ID, file):
        response = []
        for val in data.columns:
            response.append({"name": val, "value": StatisticFunctions.find_mean(val, data)})
        Acceptor.remove_file(fields['data_ID'], file)
        response = {'result': response}
        DataBaseConnection.update_result(json.dumps(response), ID)

    @staticmethod
    async def get_median(request):
        fields = await DataConverter.json(request)
        data, file = await DataConverter.convert_data(fields)
        user = User(fields["user"])
        if user.conduct_researches:
            ID = DataBaseConnection.create_empty_result(fields, "median")
            thread = Thread(target=Acceptor.get_median_con, args=(data, fields, ID, file))
            thread.start()
            response = {"ID": ID}
            return web.json_response(response)
        else:
            Validator.throw_exception(403, "No permission for conducting researches")

    @staticmethod
    def get_median_con(data, fields, ID, file):
        response = []
        for val in data.columns:
            response.append({"name": val, "value": StatisticFunctions.find_median(val, data)})
        Acceptor.remove_file(fields['data_ID'], file)
        response = {'result': response}
        DataBaseConnection.update_result(json.dumps(response), ID)

    @staticmethod
    async def get_dispersion(request):
        fields = await DataConverter.json(request)
        data, file = await DataConverter.convert_data(fields)
        user = User(fields["user"])
        if user.conduct_researches:
            ID = DataBaseConnection.create_empty_result(fields, "dispersion")
            thread = Thread(target=Acceptor.get_dispersion_con, args=(data, fields, ID, file))
            thread.start()
            response = {"ID": ID}
            return web.json_response(response)
        else:
            Validator.throw_exception(403, "No permission for conducting researches")

    @staticmethod
    def get_dispersion_con(data, fields, ID, file):
        response = []
        for val in data.columns:
            response.append({"name": val, "value": StatisticFunctions.find_dispersion(val, data)})
        Acceptor.remove_file(fields['data_ID'], file)
        response = {'result': response}
        DataBaseConnection.update_result(json.dumps(response), ID)

    @staticmethod
    async def get_std(request):
        fields = await DataConverter.json(request)
        data, file = await DataConverter.convert_data(fields)
        user = User(fields["user"])
        if user.conduct_researches:
            ID = DataBaseConnection.create_empty_result(fields, "std")
            thread = Thread(target=Acceptor.get_std_con, args=(data, fields, ID, file))
            thread.start()
            response = {"ID": ID}
            return web.json_response(response)
        else:
            Validator.throw_exception(403, "No permission for conducting researches")

    @staticmethod
    def get_std_con(data, fields, ID, file):
        response = []
        for val in data.columns:
            response.append({"name": val, "value": StatisticFunctions.find_std(val, data)})
        Acceptor.remove_file(fields['data_ID'], file)
        response = {'result': response}
        DataBaseConnection.update_result(json.dumps(response), ID)

    @staticmethod
    async def get_corr(request):
        fields = await DataConverter.json(request)
        data, file = await DataConverter.convert_data(fields)
        user = User(fields["user"])
        if user.conduct_researches:
            ID = DataBaseConnection.create_empty_result(fields, "corr")
            thread = Thread(target=Acceptor.get_corr_con, args=(data, fields, ID, file))
            thread.start()
            response = {"ID": ID}
            return web.json_response(response)
        else:
            Validator.throw_exception(403, "No permission for conducting researches")

    @staticmethod
    def get_corr_con(data, fields, ID, file):
        response = []
        method = None
        if "method" in fields:
            method = fields["method"]
        for val in data.columns:
            if method:
                response.append({"name": val, "value": StatisticFunctions.find_corr(val, data, method)})
            else:
                response.append({"name": val, "value": StatisticFunctions.find_corr(val, data)})
        Acceptor.remove_file(fields['data_ID'], file)
        response = {'result': response}
        DataBaseConnection.update_result(json.dumps(response), ID)

    @staticmethod
    async def remove_coll_cols(request):
        fields = await DataConverter.json(request)
        data, file = await DataConverter.convert_data(fields)
        user = User(fields["user"])
        if user.conduct_researches and user.edit_data:
            ID = DataBaseConnection.create_empty_result(fields, "remove_coll_cols")
            thread = Thread(target=Acceptor.remove_coll_cols_con, args=(data, fields, ID, file, user))
            thread.start()
            response = {"ID": ID}
            return web.json_response(response)
        else:
            Validator.throw_exception(403, "No permission for conducting researches or adding data")

    @staticmethod
    def remove_coll_cols_con(data, fields, ID, file, user):
        data_class = data[fields["method"]["fields"]]
        data = data.drop(columns=fields["method"]["fields"])
        method = None
        if "method" in fields["method"]:
            method = fields["method"]["method"]
        if method:
            data = DataAnalyzer.remove_collinear_features(data, fields["method"]["threshold"], method)
        else:
            data = DataAnalyzer.remove_collinear_features(data, fields["method"]["threshold"])
        data[fields["method"]["fields"]] = data_class
        save_data = ",".join(data.columns) + "\n"
        for value in data.values:
            string = ""
            for val in value:
                string += str(val)
                if val != value[-1]:
                    string += ","
            save_data += string
            if not np.array_equal(data.values[-1], value):
                save_data += "\n"
        ID_response = DataBaseConnection.save_file(save_data, None, user)
        name = DataBaseConnection.get_data_name(fields['data_ID'])
        DataBaseConnection.set_data_name(ID_response, name)
        response = {"ID": ID_response}
        Acceptor.remove_file(fields['data_ID'], file)
        DataBaseConnection.update_result(json.dumps(response), ID)

    @staticmethod
    async def remove_unimportant_cols(request):
        fields = await DataConverter.json(request)
        data, file = await DataConverter.convert_data(fields)
        user = User(fields["user"])
        if user.conduct_researches and user.edit_data:
            ID = DataBaseConnection.create_empty_result(fields, "remove_unimportant_cols")
            thread = Thread(target=Acceptor.remove_unimportant_cols_con, args=(data, fields, ID, file, user))
            thread.start()
            response = {"ID": ID}
            return web.json_response(response)
        else:
            Validator.throw_exception(403, "No permission for conducting researches or editing data")

    @staticmethod
    def remove_unimportant_cols_con(data, fields, ID, file, user):
        method = None
        if "method" in fields["method"]:
            method = fields["method"]["method"]
        corrs = []
        for field in fields["method"]['fields']:
            if method:
                corrs.append(StatisticFunctions.find_corr(field, data, method))
            else:
                corrs.append(StatisticFunctions.find_corr(field, data))
        data = DataAnalyzer.remove_unimportant_cols(data, corrs, fields["method"]["threshold"], fields["method"]['fields'])
        save_data = ",".join(data.columns) + "\n"
        for value in data.values:
            string = ""
            for val in value:
                string += str(val)
                if val != value[-1]:
                    string += ","
            save_data += string
            if not np.array_equal(data.values[-1], value):
                save_data += "\n"
        ID_response = DataBaseConnection.save_file(save_data, None, user)
        name = DataBaseConnection.get_data_name(fields['data_ID'])
        DataBaseConnection.set_data_name(ID_response, name)
        response = {"ID": ID_response}
        Acceptor.remove_file(fields['data_ID'], file)
        DataBaseConnection.update_result(json.dumps(response), ID)

    @staticmethod
    async def build_reg(request):
        fields = await DataConverter.json(request)
        user = User(fields["user"])
        if user.conduct_researches:
            ID = DataBaseConnection.create_empty_result(fields, fields["method"]["name"])
            if not (fields["method"]["name"] == "FullFactorPlan" or fields["method"]["name"] == "FullFactorPlanSecond"):
                data, file = await DataConverter.convert_data(fields)
                thread = Thread(target=Acceptor.build_reg_con, args=(data, fields, ID))
            else:
                thread = Thread(target=Acceptor.build_reg_con, args=(None, fields, ID))
            thread.start()
            response = {"ID": ID}
            return web.json_response(response)
        else:
            Validator.throw_exception(403, "No permission for conducting researches")

    @staticmethod
    def build_reg_con(data, fields, ID):
        try:
            model_ID = None
            result = None
            coefs = None
            parameters = None
            if fields["method"]["name"] == "FullFactorPlan":
                data = DataConverter.get_plan_data(fields["data_ID"])
                model, result, coefs = DataAnalyzer.build_reg_plan(data, ID)
                model_ID = DataBaseConnection.save_model(json.dumps({"coefs": coefs}), fields["data_ID"])
                PlanModel(model, model_ID, fields["user"]["ID"])
            elif fields["method"]["name"] == "FullFactorPlanSecond":
                data = DataConverter.get_plan_data_second(fields["data_ID"])
                model, result, coefs = DataAnalyzer.build_reg_plan_second(data)
                model_ID = DataBaseConnection.save_model(json.dumps({"coefs": coefs}), fields["data_ID"])
                PlanModel(model, model_ID, fields["user"]["ID"])
            else:
                model, result, coefs, parameters = DataAnalyzer.build_reg(data, fields, ID)
                model_ID = DataBaseConnection.save_model(json.dumps({"coefs": coefs}), fields["data_ID"])
                Model(model, model_ID, fields["user"]["ID"])
            response = {"model_ID": model_ID, "result": result, "coefs": coefs}
            if parameters is not []:
                response.update({"parameters": parameters})
            DataBaseConnection.update_result(json.dumps(response), ID)
        except Exception as e:
            Validator.throw_exception(500, str(e), ID)

    @staticmethod
    async def count_func(request):
        fields = await DataConverter.json(request)
        user = User(fields["user"])
        if user.add_output_data:
            model_for_work = None
            for model in Model.models_array:
                if model.id == fields["model_ID"]:
                    model_for_work = model
            if model_for_work == None:
                Validator.throw_exception(400, "No model with such ID")
            result_array = None
            if len(fields["data"]) == 0:
                data, file = await DataConverter.convert_data(fields)
                result_array = model_for_work.count_func(data)
            else:
                pdframe = pd.DataFrame()
                for field in fields["data"]:
                    pdframe[field["name"]] = field["values"]
                result_array = model_for_work.count_func(pdframe)
            response = {"result": result_array}
            return web.json_response(response)
        else:
            Validator.throw_exception(403, "No permission for count functions")

    @staticmethod
    async def get_result(request):
        fields = await DataConverter.json(request)
        user = User(fields["user"])
        response = DataBaseConnection.get_result(fields["result_ID"], user)
        return web.json_response(response)

    @staticmethod
    async def fix_data(request):
        fields = await DataConverter.json(request)
        data, file = await DataConverter.convert_data(fields)
        user = User(fields["user"])
        if user.edit_data:
            ID = DataBaseConnection.create_empty_result(fields, fields["method"]["name"])
            thread = Thread(target=Acceptor.fix_data_con, args=(data, fields, ID, file, user))
            thread.start()
            response = {"ID": ID}
            return web.json_response(response)
        else:
            Validator.throw_exception(403, "No permission for editing data")

    @staticmethod
    def fix_data_con(data, fields, ID, file, user):
        new_data = None
        if fields["method"]["name"] == "replace_nulls":
            new_data = DataFixer.replace_nulls(data, fields, ID)
        if fields["method"]["name"] == "add_data":
            new_data = DataFixer.add_data(data, fields, ID)
        save_data = ",".join(new_data.columns) + "\n"
        for value in new_data.values:
            string = ""
            for val in value:
                string += str(val)
                if val != value[-1]:
                    string += ","
            save_data += string
            if not np.array_equal(new_data.values[-1], value):
                save_data += "\n"
        new_data_ID = DataBaseConnection.save_file(save_data, None, user)
        name = DataBaseConnection.get_data_name(fields["data_ID"])
        DataBaseConnection.set_data_name(new_data_ID, name)
        response = {"ID": new_data_ID}
        Acceptor.remove_file(fields['data_ID'], file)
        DataBaseConnection.update_result(json.dumps(response), ID)

    @staticmethod
    async def remove_outliers(request):
        fields = await DataConverter.json(request)
        data, file = await DataConverter.convert_data(fields)
        user = User(fields["user"])
        if user.edit_data:
            ID = DataBaseConnection.create_empty_result(fields, "remove_outliers")
            thread = Thread(target=Acceptor.remove_outliers_con, args=(data, fields, ID, file, user))
            thread.start()
            response = {"ID": ID}
            return web.json_response(response)
        else:
            Validator.throw_exception(403, "No permission for editing data")

    @staticmethod
    def remove_outliers_con(data, fields, ID, file, user):
        new_data = DataFixer.remove_outliers(data, fields, ID)
        save_data = ",".join(new_data.columns) + "\n"
        for value in new_data.values:
            string = ""
            for val in value:
                string += str(val)
                if val != value[-1]:
                    string += ","
            save_data += string
            if not np.array_equal(new_data.values[-1], value):
                save_data += "\n"
        new_data_ID = DataBaseConnection.save_file(save_data, None, user)
        name = DataBaseConnection.get_data_name(fields["data_ID"])
        DataBaseConnection.set_data_name(new_data_ID, name)
        response = {"ID": new_data_ID}
        Acceptor.remove_file(fields['data_ID'], file)
        DataBaseConnection.update_result(json.dumps(response), ID)

    @staticmethod
    async def add_data(request):
        fields = await DataConverter.json(request)
        data, file = await DataConverter.convert_data(fields)
        user = User(fields["user"])
        if user.edit_data:
            ID = DataBaseConnection.create_empty_result(fields, "add_data")
            thread = Thread(target=Acceptor.add_data_con, args=(data, fields, ID, file, user))
            thread.start()
            response = {"ID": ID}
            return web.json_response(response)
        else:
            Validator.throw_exception(403, "No permission for editing data")

    @staticmethod
    def add_data_con(data, fields, ID, file, user):
        new_data = DataConverter.add_data(data, fields, ID)
        save_data = ",".join(new_data.columns) + "\n"
        for value in new_data.values:
            string = ""
            for val in value:
                string += str(val)
                if val != value[-1]:
                    string += ","
            save_data += string
            if not np.array_equal(new_data.values[-1], value):
                save_data += "\n"
        new_data_ID = DataBaseConnection.save_file(save_data, None, user)
        name = DataBaseConnection.get_data_name(ID)
        DataBaseConnection.set_data_name(new_data_ID, name)
        response = {"ID": new_data_ID}
        Acceptor.remove_file(fields['data_ID'], file)
        DataBaseConnection.update_result(json.dumps(response), ID)

    @staticmethod
    async def save_file(request):
        fields = await DataConverter.json(request)
        user = User(fields["user"])
        if user.add_data:
            response = await DataConverter.save_data(request, user, True)
            response = {'ID': response}
            return web.json_response(response)
        else:
            Validator.throw_exception(403, "No permission for adding data")

    @staticmethod
    async def save_plan_data(request):
        fields = await DataConverter.json(request)
        user = User(fields["user"])
        if user.add_data:
            response = await DataConverter.save_plan_data(request, user)
            response = {'ID': response}
            return web.json_response(response)
        else:
            Validator.throw_exception(403, "No permission for adding data")

    @staticmethod
    def remove_file(ID, file):
        file.close()
        if os.path.exists("./csvFiles/file" + str(ID) + ".csv"):
            os.remove("./csvFiles/file" + str(ID) + ".csv")
        else:
            print("The file does not exist")

    @staticmethod
    async def get_all_data(request):
        fields = await DataConverter.json(request)
        user = User(fields["user"])
        response = DataBaseConnection.get_all_data(user.ID)
        response_json = []
        for resp in response:
            response_json.append({"name": DataBaseConnection.get_data_name(resp[0]), "value": resp[0], "last_fix_date": resp[1].strftime("%d.%m.%Y %H:%M:%S"), "creation_date": resp[2].strftime("%d.%m.%Y %H:%M:%S")})
        response = {"result": response_json}
        return web.json_response(response)

    @staticmethod
    async def get_data(request):
        fields = await DataConverter.json(request)
        user = User(fields["user"])
        response, csv = await DataConverter.get_data(fields["ID"])
        analyze_results = DataBaseConnection.get_analyze_results(fields["ID"])
        name = DataBaseConnection.get_data_name(fields["ID"])
        response_json = None
        if csv:
            response_json = {"result": response.encode("utf-8").hex(), 'analyze_results': analyze_results, "name": name}
        else:
            response_json = {"result": response, 'analyze_results': analyze_results, "name": name}
        return web.json_response(response_json)

    @staticmethod
    async def get_token(request):
        m = hashlib.sha256()
        m.update("1".encode("utf8"))
        # TODO: remove passw
        random_generator = Random.new().read
        private_key = RSA.generate(1024, random_generator)

        public_key = private_key.publickey()
        private_key_exp = private_key.export_key(format='PEM', passphrase=None, pkcs=8)
        public_key_exp = private_key.publickey().export_key(format='PEM', passphrase=None, pkcs=8)
        ID = DataBaseConnection.create_new_session(private_key_exp.decode("utf-8"), public_key_exp.decode("utf-8"))
        public_key = PKCS1_OAEP.new(public_key)
        passw = public_key.encrypt(m.hexdigest().encode("utf8")).hex()
        response = {"ID": ID, "key": public_key_exp.decode("utf-8"), "password": passw}
        return web.json_response(response)

    @staticmethod
    async def activate_token(request):
        fields = await DataConverter.json(request)
        public_key, private_key = DataBaseConnection.get_session(fields["user"]["ID"])
        privkey = RSA.importKey(private_key.encode('utf-8'))
        privkey = PKCS1_OAEP.new(privkey)
        user_data, role_data = DataBaseConnection.get_user_and_role_by_login(fields["user"]["login"], privkey.decrypt(
            bytearray.fromhex(fields["user"]["password"])).decode("utf8"))
        DataBaseConnection.activate_token(fields["user"]["ID"], user_data[1])
        response = {'result': "successful",
                    'user': {
                        'login': fields["user"]["login"],
                        'last_name': user_data[2],
                        'first_name': user_data[3],
                        'patronymic': user_data[4]
                    },
                    'role': {
                        'role_name': role_data[5],
                        'description': role_data[0],
                        'add_data': role_data[2],
                        'add_output_data': role_data[3],
                        'edit_data': role_data[1],
                        'conduct_researches': role_data[4]
                    }
                }
        return web.json_response(response)
