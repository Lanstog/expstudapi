import numpy as np

from validator import Validator


class StatisticFunctions:
    @staticmethod
    def find_min(field, data):
        min = np.min(data[field])
        return float(min)

    @staticmethod
    def find_max(field, data):
        max = np.max(data[field])
        return float(max)

    @staticmethod
    def find_mean(field, data):
        mean = np.mean(data[field])
        return float(mean)

    @staticmethod
    def find_median(field, data):
        med = np.median(data[field])
        return float(med)

    @staticmethod
    def find_dispersion(field, data):
        disp = np.var(data[field])
        return float(disp)

    @staticmethod
    def find_std(field, data):
        std = np.std(data[field])
        return float(std)

    @staticmethod
    def find_corr(field, data, method='pearson'):
        correlations_data = data.corr(method=method)[field].sort_values()
        result_array = " ".join(str(correlations_data).split())
        result_array = result_array.split(" ")
        result_output = []
        for i in range(0, len(result_array) - 6, 2):
            result = {
                "name": result_array[i],
                "value": result_array[i + 1]
            }
            result_output.append(result)
        return result_output
