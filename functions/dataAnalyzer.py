import math
import scipy.stats
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LogisticRegression, LinearRegression, Ridge, Lasso
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR
import numpy as np
from sklearn.tree import DecisionTreeRegressor, DecisionTreeClassifier, export_text, export_graphviz

from classes.plan_entities.model_parameter import ModelParameter
from validator import Validator


class DataAnalyzer:
    @staticmethod
    def remove_collinear_features(x, threshold, method='pearson'):
        corr_matrix = x.corr(method=method)
        iters = range(len(corr_matrix.columns) - 1)
        drop_cols = []

        for i in iters:
            for j in range(i):
                item = corr_matrix.iloc[j:(j + 1), (i + 1):(i + 2)]
                col = item.columns
                val = abs(item.values)

                if val >= threshold:
                    drop_cols.append(col.values[0])

        drops = set(drop_cols)
        x = x.drop(columns=drops)
        return x

    @staticmethod
    def remove_unimportant_cols(data, corrs, threshold, fields):
        drop_cols = []
        for value_row in corrs:
            for value in value_row:
                if math.fabs(float(value['value'])) <= threshold and value['name'] not in drop_cols and value['name'] not in fields:
                    drop_cols.append(value['name'])
        drops = set(drop_cols)
        data = data.drop(columns=drops)
        return data

    @staticmethod
    def build_reg(data, fields, ID):
        if "field" not in fields:
            Validator.throw_exception(400, "No field: field", ID)
        if "test_size" not in fields:
            Validator.throw_exception(400, "No field: test_size", ID)
        if "name" not in fields["method"]:
            Validator.throw_exception(400, "No field: name", ID)
        field = fields["field"]
        method = fields["method"]["name"]
        target = data[[field]]
        data = data.drop(field, axis=1)
        x_train, x_test, y_train, y_test = train_test_split(data, target, test_size=fields["test_size"])

        model = None
        if "cross_validation" in fields["method"]:
            if method == "linear":
                model = LinearRegression()
            elif method == "lasso":
                model = Lasso()
            elif method == "tree_regressor":
                model = DecisionTreeRegressor()
            elif method == "tree_classifier":
                model = DecisionTreeClassifier()
            elif method == "forest_regressor":
                model = RandomForestRegressor()
            elif method == "forest_classifier":
                model = RandomForestClassifier()
            elif method == "neighbors":
                model = KNeighborsRegressor()
            elif method == "svr":
                model = SVR()
            elif method == "logistic":
                model = LogisticRegression()
            elif method == "ols":
                model = Ridge()
            else:
                Validator.throw_exception(400, "Unknown method", ID)
            parameters = {}
            for val in fields["method"]["cross_validation"]:
                parameters.update({val["name"]: val["values"]})
            model = GridSearchCV(model, parameters)
        else:
            if method == "linear":
                model = LinearRegression()
            elif method == "lasso":
                if "alpha" not in fields["method"]:
                    Validator.throw_exception(400, "No field: alpha", ID)
                model = Lasso(alpha=fields["method"]["alpha"])
            elif method == "tree_regressor":
                if "criterion" not in fields["method"]:
                    Validator.throw_exception(400, "No field: criterion", ID)
                if "max_depth" not in fields["method"]:
                    Validator.throw_exception(400, "No field: max_depth", ID)
                if "min_samples_split" not in fields["method"]:
                    Validator.throw_exception(400, "No field: min_samples_split", ID)
                if "min_samples_leaf" not in fields["method"]:
                    Validator.throw_exception(400, "No field: min_samples_leaf", ID)
                model = DecisionTreeRegressor(criterion=fields["method"]["criterion"], max_depth=fields["method"]["max_depth"], min_samples_split=fields["method"]["min_samples_split"], min_samples_leaf=fields["method"]["min_samples_leaf"])
            elif method == "tree_classifier":
                if "criterion" not in fields["method"]:
                    Validator.throw_exception(400, "No field: criterion", ID)
                if "max_depth" not in fields["method"]:
                    Validator.throw_exception(400, "No field: max_depth", ID)
                if "min_samples_split" not in fields["method"]:
                    Validator.throw_exception(400, "No field: min_samples_split", ID)
                if "min_samples_leaf" not in fields["method"]:
                    Validator.throw_exception(400, "No field: min_samples_leaf", ID)
                model = DecisionTreeClassifier(criterion=fields["method"]["criterion"], max_depth=fields["method"]["max_depth"], min_samples_split=fields["method"]["min_samples_split"], min_samples_leaf=fields["method"]["min_samples_leaf"])
            elif method == "forest_regressor":
                if "estimators" not in fields["method"]:
                    Validator.throw_exception(400, "No field: estimators", ID)
                if "criterion" not in fields["method"]:
                    Validator.throw_exception(400, "No field: criterion", ID)
                if "max_depth" not in fields["method"]:
                    Validator.throw_exception(400, "No field: max_depth", ID)
                if "min_samples_split" not in fields["method"]:
                    Validator.throw_exception(400, "No field: min_samples_split", ID)
                if "min_samples_leaf" not in fields["method"]:
                    Validator.throw_exception(400, "No field: min_samples_leaf", ID)
                model = RandomForestRegressor(n_estimators=fields["method"]["estimators"],
                                              criterion=fields["method"]["criterion"],
                                              max_depth=fields["method"]['max_depth'],
                                              min_samples_split=fields["method"]["min_samples_split"],
                                              min_samples_leaf=fields["method"]["min_samples_leaf"])
            elif method == "forest_classifier":
                if "estimators" not in fields["method"]:
                    Validator.throw_exception(400, "No field: estimators", ID)
                if "criterion" not in fields["method"]:
                    Validator.throw_exception(400, "No field: criterion", ID)
                if "max_depth" not in fields["method"]:
                    Validator.throw_exception(400, "No field: max_depth", ID)
                if "min_samples_split" not in fields["method"]:
                    Validator.throw_exception(400, "No field: min_samples_split", ID)
                if "min_samples_leaf" not in fields["method"]:
                    Validator.throw_exception(400, "No field: min_samples_leaf", ID)
                model = RandomForestClassifier(n_estimators=fields["method"]["estimators"],
                                              criterion=fields["method"]["criterion"],
                                              max_depth=fields["method"]['max_depth'],
                                              min_samples_split=fields["method"]["min_samples_split"],
                                              min_samples_leaf=fields["method"]["min_samples_leaf"])
            elif method == "neighbors":
                if "neighbors" not in fields["method"]:
                    Validator.throw_exception(400, "No field: neighbors", ID)
                model = KNeighborsRegressor(n_neighbors=fields["method"]["neighbors"])
            elif method == "svr":
                if "kernel" not in fields["method"]:
                    Validator.throw_exception(400, "No field: kernel", ID)
                model = SVR(kernel=fields["method"]['kernel'])
            elif method == "logistic":
                model = LogisticRegression()
            elif method == "ols":
                if "alpha" not in fields["method"]:
                    Validator.throw_exception(400, "No field: alpha", ID)
                model = Ridge(alpha=fields["method"]["alpha"])
            else:
                Validator.throw_exception(400, "Unknown method", ID)

        model.fit(x_train, y_train.values.ravel())
        parameters = []
        if "cross_validation" in fields["method"]:
            parameters = model.best_params_
            model = model.best_estimator_
        else:
            parameters = {}
            parameters_not_fixed = model.get_params()
            for key, val in parameters_not_fixed.items():
                if (key, val) in fields["method"].items():
                    parameters.update({key: val})
        coefs = []
        parameters.update({"class-field": fields["field"]})
        if method == "linear" or method == "ols" or method == "lasso":
            for i in range(0, len(model.coef_)):
                coefs.append({"name": data.columns.values[i],
                              "value": model.coef_[i]})
            coefs.append({"name": "BaseValue",
                          "value": model.intercept_})
        elif method == "forest_regressor" or method == "forest_classifier":
            for i in range(0, len(model.feature_importances_)):
                coefs.append({"name": data.columns.values[i],
                              "value": model.feature_importances_[i]})
        elif method == "logistic" or method == "svr":
            for i in range(0, len(model.coef_)):
                coef = []
                for j in range(0, len(model.coef_[i])):
                    coef.append({"name": data.columns.values[j],
                                 "value": model.coef_[i][j]})
                coef.append({"name": "BaseValue",
                             "value": model.intercept_[i]})
                coefs.append(coef)
        elif method == "tree_regressor" or method == "tree_classifier":
            coefs = export_graphviz(model)

        return model, r2_score(y_test, model.predict(x_test)), coefs, parameters


    @staticmethod
    def build_reg_plan(plan, ID):
        parameters = plan.count_parameters()
        model_parameters = []
        index = 0
        model_parameters.append(ModelParameter(["1"], [], parameters[index]))
        index += 1
        start_size = len(plan.titles) - 1
        for i in range(1, len(plan.titles) + 1):
            array_indexes = []
            for j in range(0, i):
                array_indexes.append(j)
            current_index_switch = len(array_indexes) - 1
            while not current_index_switch == -1:
                titles = []
                variables = []
                for value in array_indexes:
                    titles.append(plan.titles[value])
                    variables.append(plan.parameters[value])
                model_parameters.append(ModelParameter(titles, variables, parameters[index]))
                index += 1
                if i == start_size + 1:
                    break
                elif array_indexes[len(array_indexes) - 1] == start_size:
                    if array_indexes[current_index_switch] == start_size - (
                            len(array_indexes) - 1 - current_index_switch):
                        current_index_switch -= 1
                    array_indexes[current_index_switch] = array_indexes[current_index_switch] + 1
                    for j in range(current_index_switch + 1, len(array_indexes)):
                        array_indexes[j] = array_indexes[j - 1] + 1
                else:
                    array_indexes[len(array_indexes) - 1] += 1
        updated_parameters = []
        model_parameters_static = model_parameters.copy()

        s = 0
        n = len(plan.experiments)
        m = len(plan.experiments[0].output_values)
        for experiment in plan.experiments:
            mid_val = 0
            for value in experiment.output_values:
                mid_val += (value - experiment.count_mean()) ** 2
            s += mid_val / (m - 1)
        s = s / (n)
        s_coef = math.sqrt(s / (n * m))
        student = scipy.stats.t.ppf(1.0 - (plan.a/2), n * (m - 1))
        s_comp = s_coef * student
        removed_values = []
        for parameter in model_parameters:
            if s_comp > abs(parameter.value):
                for i in range(0, len(model_parameters)):
                    if model_parameters[i] == parameter:
                        removed_values.append(i)
                        break
                model_parameters.remove(parameter)

        fisher_array = []
        for k in range(0, len(plan.experiments)):
            value = 0
            for i in range(0, len(model_parameters_static)):
                removed = False
                for j in range(0, len(removed_values)):
                    if i == removed_values[j]:
                        removed = True
                        break
                if removed:
                    continue
                if i == 0:
                    value += model_parameters_static[i].value
                else:
                    if plan.experiments[k].parameters[i-1]:
                        value += model_parameters_static[i].value
                    else:
                        value -= model_parameters_static[i].value
            fisher_array.append(value)

        r = len(model_parameters)
        s_ost = 0
        for k in range(0, len(fisher_array)):
            s_ost = s_ost + (fisher_array[k] - plan.experiments[k].count_mean()) ** 2
        try:
            s_ost = s_ost * m / (n - r)
        except Exception:
            Validator.throw_exception(400, "No important features", ID)
        fisher = s_ost / s
        fisher_table = scipy.stats.f.ppf(1.0 - plan.a, n - r, n * (m - 1))
        model_is_correct = False
        if fisher < fisher_table:
            model_is_correct = True
        for i in range(0, len(model_parameters_static)):
            val = 0
            for j in range(0, len(model_parameters)):
                if model_parameters[j].get_coef(model_parameters_static[i].titles) is not None:
                    val += model_parameters[j].get_coef(model_parameters_static[i].titles)
            updated_parameters.append(val)
        coefs = []
        for i in range(0, len(updated_parameters)):
            model_parameters_static[i].value = updated_parameters[i]
            total_title = ""
            for j in range(0, len(model_parameters_static[i].titles)):
                total_title += model_parameters_static[i].titles[j]
                if not j == len(model_parameters_static[i].titles) - 1:
                    total_title += ", "
            coefs.append({
                "name": total_title,
                "value": float(model_parameters_static[i].value)
            })
        return model_parameters_static, model_is_correct, coefs

    @staticmethod
    def build_reg_plan_second(plan):
        parameters = plan.count_parameters()
        model_parameters = []
        index = 0
        model_parameters.append(ModelParameter(["1"], [], parameters[index]))
        index += 1
        start_size = len(plan.titles) - 1
        for i in range(1, len(plan.titles) + 1):
            array_indexes = []
            for j in range(0, i):
                array_indexes.append(j)
            current_index_switch = len(array_indexes) - 1
            while not current_index_switch == -1:
                titles = []
                variables = []
                for value in array_indexes:
                    titles.append(plan.titles[value])
                    variables.append(plan.parameters[value])
                model_parameters.append(ModelParameter(titles, variables, parameters[index]))
                index += 1
                if i == start_size + 1:
                    break
                elif array_indexes[len(array_indexes) - 1] == start_size:
                    if array_indexes[current_index_switch] == start_size - (
                            len(array_indexes) - 1 - current_index_switch):
                        current_index_switch -= 1
                    array_indexes[current_index_switch] = array_indexes[current_index_switch] + 1
                    for j in range(current_index_switch + 1, len(array_indexes)):
                        array_indexes[j] = array_indexes[j - 1] + 1
                else:
                    array_indexes[len(array_indexes) - 1] += 1
        counter = 0
        while True:
            if len(parameters) == len(model_parameters):
                break
            else:
                model_parameters.append(ModelParameter([plan.titles[counter], plan.titles[counter]], [plan.parameters[counter], plan.parameters[counter]], parameters[len(model_parameters)]))
                counter += 1
        updated_parameters = []
        model_parameters_static = model_parameters.copy()

        s = 0
        n = len(plan.experiments)
        m = len(plan.experiments[0].output_values)
        for experiment in plan.experiments:
            mid_val = 0
            for value in experiment.output_values:
                mid_val += (value - experiment.count_mean()) ** 2
            s += mid_val / (m-1)
        s = s / (n)
        s_coef = math.sqrt(s / (n * m))
        student = scipy.stats.t.ppf(1.0 - (plan.a / 2), n * (m - 1))
        s_comp = s_coef * student
        removed_values = []
        back = 0
        for j in range(0, len(model_parameters)):
            j = j - back
            parameter = model_parameters[j]
            if s_comp > abs(parameter.value):
                for i in range(0, len(model_parameters)):
                    if model_parameters[i] == parameter:
                        removed_values.append(i)
                        break
                model_parameters.remove(parameter)
                back += 1

        fisher_array = []
        for k in range(0, len(plan.experiments)):
            value = 0
            for i in range(0, len(model_parameters_static)):
                removed = False
                for j in range(0, len(removed_values)):
                    if i == removed_values[j]:
                        removed = True
                        break
                if removed:
                    continue
                if i == 0:
                    value += model_parameters_static[i].value
                else:
                    if plan.experiments[k].parameters[i - 1]:
                        value += model_parameters_static[i].value
                    else:
                        value -= model_parameters_static[i].value
            fisher_array.append(value)

        r = len(model_parameters)
        s_ost = 0
        for k in range(0, len(fisher_array)):
            s_ost = s_ost + (fisher_array[k] - plan.experiments[k].count_mean()) ** 2
        s_ost = s_ost * m / (n - r)
        fisher = s_ost / s
        fisher_table = scipy.stats.f.ppf(1.0 - plan.a, n - r, n * (m - 1))
        model_is_correct = False
        if fisher < fisher_table:
            model_is_correct = True
        for i in range(0, len(model_parameters_static)):
            val = 0
            for j in range(0, len(model_parameters)):
                if model_parameters[j].get_coef(model_parameters_static[i].titles) is not None:
                    val += model_parameters[j].get_coef(model_parameters_static[i].titles)
            updated_parameters.append(val)
        coefs = []
        for i in range(0, len(updated_parameters)):
            model_parameters_static[i].value = updated_parameters[i]
            total_title = ""
            for j in range(0, len(model_parameters_static[i].titles)):
                total_title += model_parameters_static[i].titles[j]
                if not j == len(model_parameters_static[i].titles) - 1:
                    total_title += ", "
            coefs.append({
                "name": total_title,
                "value": float(model_parameters_static[i].value)
            })
        return model_parameters_static, model_is_correct, coefs