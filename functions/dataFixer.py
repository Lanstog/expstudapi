import pandas as pd
import numpy as np

from functions.statisticFunctions import StatisticFunctions
from validator import Validator


class DataFixer:
    @staticmethod
    def replace_nulls(data, fields, ID):
        if "method" not in fields:
            Validator.throw_exception(400, "No field: method", ID)
        if "replace_method" not in fields['method']:
            Validator.throw_exception(400, "No field: replace_method", ID)
        if "fields_to_fix" not in fields['method']:
            Validator.throw_exception(400, "No field: fields_to_fix", ID)
        if "class_fields" not in fields['method']:
            Validator.throw_exception(400, "No field: class_fields", ID)
        new_data = pd.DataFrame()
        if fields["method"]["replace_method"] == "mean":
            spt = [pd.DataFrame(y) for x, y in data.groupby(fields["method"]["class_fields"], as_index=False)]
            for data_part in spt:
                null_frame = data_part.isnull()
                for field in fields["method"]["fields_to_fix"]:
                    mean_val = StatisticFunctions.find_mean(field, data_part)
                    for i in data_part.index:
                        if null_frame[field][i]:
                            data_part[field][i] = mean_val
                new_data = new_data.append(data_part)
        elif fields["method"]["replace_method"] == "random_mean":
            spt = [pd.DataFrame(y) for x, y in data.groupby(fields["method"]["class_fields"], as_index=False)]
            for data_part in spt:
                null_frame = data_part.isnull()
                for field in fields["method"]["fields_to_fix"]:
                    mean_val = StatisticFunctions.find_mean(field, data_part)
                    std = StatisticFunctions.find_std(field, data_part)
                    for i in data_part.index:
                        if null_frame[field][i]:
                            data_part[field][i] = np.random.normal(mean_val, std, 1)[0]
                new_data = new_data.append(data_part)
        else:
            Validator.throw_exception(400, "Unknown method", ID)
        return new_data

    @staticmethod
    def remove_outliers(data, fields, ID):
        if "method" not in fields:
            Validator.throw_exception(400, "No field: method", ID)
        if "k" not in fields['method']:
            Validator.throw_exception(400, "No field: k", ID)
        if "fields_to_fix" not in fields['method']:
            Validator.throw_exception(400, "No field: fields_to_fix", ID)
        if "class_fields" not in fields['method']:
            Validator.throw_exception(400, "No field: class_fields", ID)
        new_data = pd.DataFrame()
        spt = [pd.DataFrame(y) for x, y in data.groupby(fields["method"]["class_fields"], as_index=False)]
        for data_part in spt:
            for field in fields["method"]["fields_to_fix"]:
                q25 = np.percentile(data_part[field], 25)
                q75 = np.percentile(data_part[field], 75)
                iqr = q75 - q25
                cut_off_index = iqr * fields["method"]["k"]
                lower_val = q25 - cut_off_index
                upper_val = q75 + cut_off_index
                filter = data_part[field] > lower_val
                data_part = data_part[filter]
                filter = data_part[field] < upper_val
                data_part = data_part[filter]
            new_data = new_data.append(data_part)
        return new_data

    @staticmethod
    def add_data(data, fields, ID):
        if "method" not in fields:
            Validator.throw_exception(400, "No field: method", ID)
        if "replace_method" not in fields['method']:
            Validator.throw_exception(400, "No field: replace_method", ID)
        if "count" not in fields['method']:
            Validator.throw_exception(400, "No field: fields_to_fix", ID)
        if "class_fields" not in fields['method']:
            Validator.throw_exception(400, "No field: class_fields", ID)
        new_data = pd.DataFrame()
        class_fields = fields["method"]["class_fields"]
        counts = fields['method']['count']
        if fields["method"]["replace_method"] == "mean":
            spt = [pd.DataFrame(y) for x, y in data.groupby(class_fields, as_index=False)]
            for data_part in spt:
                #if len(counts) != 0:
                    count, counts = DataFixer.get_add_size(counts, data_part[class_fields], data_part.index)
                    if count is not None:
                        for i in range(0, count):
                            new_row = pd.DataFrame(columns=data_part.columns, index=[0])
                            for field in data_part.columns:
                                new_row[field][0] = StatisticFunctions.find_mean(field, data_part)
                            data_part = data_part.append(new_row)
                    new_data = new_data.append(data_part)
        elif fields["method"]["replace_method"] == "random_mean":
            spt = [pd.DataFrame(y) for x, y in data.groupby(class_fields, as_index=False)]
            for data_part in spt:
                #if len(counts) != 0:
                    count, counts = DataFixer.get_add_size(counts, data_part[class_fields], data_part.index)
                    if count is not None:
                        for i in range(0, count):
                            new_row = pd.DataFrame(columns=data_part.columns, index=[0])
                            for field in data_part.columns:
                                mean_val = StatisticFunctions.find_mean(field, data_part)
                                std = StatisticFunctions.find_std(field, data_part)
                                new_row[field][0] = np.random.normal(mean_val, std, 1)[0]
                            data_part = data_part.append(new_row)
                    new_data = new_data.append(data_part)
        else:
            Validator.throw_exception(400, "Unknown method", ID)
        if len(counts) != 0:
            Validator.throw_exception(400, "Some data was not added, fix class_fields", ID)
        return new_data

    @staticmethod
    def get_add_size(fields, value, index):
        for k in range(0, len(fields)):
            for i in index:
                exist = True
                for j in range(0, value.shape[1]):
                    column = value.columns[j]
                    if fields[k]['value'][j] != value[column][i]:
                        exist = False
                        break
                if exist:
                    fields_copy = []
                    for j in range(0, len(fields)):
                        if j != k:
                            fields_copy.append(fields[j])
                    return fields[k]['count'], fields_copy
        return None, fields
